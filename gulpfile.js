var elixir = require('laravel-elixir');
elixir.config.autoprefix = true;
elixir.config.production = true;
elixir.config.sourcemaps = true;
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix
        /* bootstrap */
        .copy('bower_components/bootstrap/less', 'resources/assets/bootstrap')
        // .copy('bower_components/bootstrap/fonts', 'public/fonts/')
        /* bootstrap */

        /* bootstrap-social */
        .copy('bower_components/bootstrap-social/bootstrap-social.less', 'resources/assets/bootstrap-social/bootstrap-social.less')
        /* bootstrap-social */

        /* datatables */
        .copy('bower_components/datatables/media/css/jquery.dataTables.min.css', 'resources/assets/datatables/jquery.dataTables.min.less')
        .copy('bower_components/datatables/media/css/jquery.dataTables_themeroller.min.css', 'resources/assets/datatables/jquery.dataTables_themeroller.min.less')
        // .copy('bower_components/datatables/media/images', 'resources/assets/images')
        // .copy('bower_components/datatables/media/images', 'public/images')
        /* datatables */

        /* datatables-responsive */
        .copy('bower_components/datatables-responsive/css/dataTables.responsive.less', 'resources/assets/datatables/jquery.dataTables.min.less')
        /* datatables-responsive */

        /* mentisMenu */
        .copy('bower_components/metisMenu/dist/metisMenu.min.css', 'resources/assets/metisMenu/metisMenu.min.less')
        /* mentisMenu */

        /* mentisMenu */
        .copy('bower_components/morrisjs/less/morris.core.less', 'resources/assets/morrisjs/morris.core.less')
        /* mentisMenu */

        /* font-awesome */
        // .copy('bower_components/font-awesome/fonts', 'public/fonts/')
        .copy('bower_components/font-awesome/less', 'resources/assets/font-awesome')
        /* font-awesome */

        /* themes */
        .copy('bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css', 'resources/assets/startbootstrap/sb-admin-2.less')
        .copy('bower_components/startbootstrap-sb-admin-2/dist/css/timeline.css', 'resources/assets/startbootstrap/timeline.less')
        /* themes */

        /* js */
        .copy('bower_components/bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js')
        .copy('bower_components/jquery/dist/jquery.min.js', 'resources/assets/js')
        .copy('bower_components/datatables/media/js/jquery.dataTables.min.js', 'resources/assets/js')
        .copy('bower_components/datatables-responsive/js/dataTables.responsive.min.js', 'resources/assets/js')
        .copy('bower_components/metisMenu/dist/metisMenu.min.js', 'resources/assets/js')
        .copy('bower_components/morrisjs/morris.js', 'resources/assets/js')
        .copy('bower_components/raphael/raphael-min.js', 'resources/assets/js')
        .copy('bower_components/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js', 'resources/assets/js')
        /* js */

        /* build folder */
        .copy('bower_components/font-awesome/fonts', 'public/build/fonts/')
        .copy('bower_components/bootstrap/fonts', 'public/build/fonts/')
        .copy('bower_components/datatables/media/images', 'public/build/images')
        /* build folder */

        /* concat js */
        .scripts([
            'jquery.min.js', 'bootstrap.min.js', 'jquery.dataTables.min.js', 'morris.js', 'raphael-min.js','metisMenu.min.js', 'sb-admin-2.js', 'custom.js'
        ])
        /* concat js */

        /* compile less */
        .less('app.less')
        /* compile less */

        /* version assets */
        .version(['css/app.css', 'js/all.js'])
    /* version assets */

});